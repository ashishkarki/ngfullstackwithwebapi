﻿// service
(function () {
    "use strict";

    angular.module("common.services")
        .factory("productResource",
        ["$resource",
            "appSettings",
            productResource
        ]);

    function productResource($resource, appSettings) {
        // second argument is default value
        // third is custom action
        return $resource(appSettings.serverPath + "/api/products/:id", null,
        {
            'update': {method: 'PUT'}
        });
    }
})();